﻿namespace GeneticAlgorithm
{
    public class Genotype
    {
        public bool IsValid { get; }

        public int Value { get; set; }
    }
}