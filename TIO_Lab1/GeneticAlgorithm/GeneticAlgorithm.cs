﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticAlgorithm.AlgorithmProviders.Crossovers;
using GeneticAlgorithm.AlgorithmProviders.Initializers;
using GeneticAlgorithm.AlgorithmProviders.Loggers;
using GeneticAlgorithm.AlgorithmProviders.Mutators;
using GeneticAlgorithm.AlgorithmProviders.Selectors;

namespace GeneticAlgorithm
{
    public class GeneticAlgorithm
    {
        private readonly ICrossover crossover;
        private readonly IInitializer initializer;
        private readonly ILogger logger;
        private readonly IMutator mutator;
        private readonly ISelector selector;

        private readonly GeneticAlgorithmConfig config;

        private int epoch;
        private List<Tuple<Genotype, Genotype>> parents;
        private List<Genotype> population;

        public GeneticAlgorithm(
            IInitializer initializer,
            ISelector selector,
            ICrossover crossover,
            IMutator mutator,
            ILogger logger,
            GeneticAlgorithmConfig config)
        {
            this.initializer = initializer;
            this.selector = selector;
            this.crossover = crossover;
            this.mutator = mutator;
            this.logger = logger;

            this.config = config;
        }

        public Genotype Run()
        {
            Initialize();
            EndEpoch();

            while (ShouldAlgorithmRun())
            {
                Select();
                Crossover();
                Mutate();
                RemoveInvalidGenotypes();
                TrimPopulation();

                EndEpoch();
            }

            return SelectBest();
        }

        private void Initialize()
        {
            population = initializer.Initialize(config.InitialPopulationCount, config.ProblemDimensions);
        }

        private void EndEpoch()
        {
            logger.LogEpoch(epoch, population);
            epoch++;
        }

        private bool ShouldAlgorithmRun()
        {
            return !config?.StopCondition(epoch, population) ?? false;
        }

        private void Select()
        {
            parents = selector.Select(population, config.ParentsPairsCount);
        }

        private void Crossover()
        {
            if (config.KeepParents)
            {
                population = crossover.Crossover(parents, config.CrossoverSplitPoints)
                    .Union(population)
                    .ToList();
            }
            else
            {
                var bestGenotype = config.KeepBestGenotype
                    ? new List<Genotype> {SelectBest()}
                    : new List<Genotype>();

                population = population = crossover.Crossover(parents, config.CrossoverSplitPoints)
                    .Union(bestGenotype)
                    .ToList();
            }
        }

        private void Mutate()
        {
            mutator.Mutate(population, config.MutationChance);
        }

        private void RemoveInvalidGenotypes()
        {
            population = population
                .Where(genotype => genotype.IsValid)
                .ToList();
        }

        private void TrimPopulation()
        {
            if (!config.TrimPopulationToInitialCount)
                return;

            population = population
                .OrderByDescending(genotype => genotype.Value)
                .Take(config.InitialPopulationCount)
                .ToList();
        }

        private Genotype SelectBest()
        {
            var bestValue = population
                .Select(genotype => genotype.Value)
                .Max();

            return population
                .FirstOrDefault(genotype => genotype.Value == bestValue);
        }
    }
}