﻿using System;
using System.Collections.Generic;

namespace GeneticAlgorithm
{
    public class GeneticAlgorithmConfig
    {
        public List<int> CrossoverSplitPoints { get; set; }

        public int InitialPopulationCount { get; set; }

        public bool KeepBestGenotype { get; set; }

        public bool KeepParents { get; set; }

        public double MutationChance { get; set; }

        public int ParentsPairsCount { get; set; }

        public List<int> ProblemDimensions { get; set; }

        public Func<int, List<Genotype>, bool> StopCondition { get; set; }

        public bool TrimPopulationToInitialCount { get; set; }
    }
}