﻿using System;
using System.Collections.Generic;

namespace GeneticAlgorithm.AlgorithmProviders.Crossovers
{
    public interface ICrossover
    {
        List<Genotype> Crossover(List<Tuple<Genotype, Genotype>> parents, List<int> splitPoints);
    }
}