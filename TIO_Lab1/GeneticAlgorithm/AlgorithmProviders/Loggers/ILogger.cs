﻿using System.Collections.Generic;

namespace GeneticAlgorithm.AlgorithmProviders.Loggers
{
    public interface ILogger
    {
        void LogEpoch(int epoch, List<Genotype> population);
    }
}