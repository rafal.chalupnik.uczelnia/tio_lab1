﻿using System;
using System.Collections.Generic;

namespace GeneticAlgorithm.AlgorithmProviders.Selectors
{
    public interface ISelector
    {
        List<Tuple<Genotype, Genotype>> Select(List<Genotype> population, int parentsPairsCount);
    }
}