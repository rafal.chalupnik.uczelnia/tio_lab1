﻿using System.Collections.Generic;

namespace GeneticAlgorithm.AlgorithmProviders.Initializers
{
    public interface IInitializer
    {
        List<Genotype> Initialize(int populationCount, List<int> problemDimensions);
    }
}