﻿using System.Collections.Generic;

namespace GeneticAlgorithm.AlgorithmProviders.Mutators
{
    public interface IMutator
    {
        void Mutate(List<Genotype> population, double mutationChance);
    }
}